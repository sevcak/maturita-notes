# Functions

In mathematics, a function **assigns** a **single output** for each of its inputs.

- from a set of $X$, a function assigns **to each element of $X$ exactly one element of $Y$
- set $X$ - **domain** of the function
- set $Y$ - **codomain** of the function 
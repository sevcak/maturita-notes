
```javascript
// objekt reprezentujúci vzorový balíček kariet
{
	id: 0,
	custom: false,
	title: "Capitals of countries",
	coverUrl: require(
		"../assets/example-decks/capitals-of-countries/cover.jpg"
	),
	cards: [
	  {
		id: 0,
		ars: 1,
		front: {
		  title: "Germany",
		  content: "What is the capital of Germany?",
		  image: undefined,
		},
		back: {
		  title: "Berlin",
		  content: undefined,
		  image: undefined,
		},
	  },
	  {
		id: 1,
		ars: 1,
		front: {
		  title: "Czech Republic",
		  content: "What is the capital of Czech Republic?",
		  image: undefined,
		},
		back: {
		  title: "Prague",
		  content: undefined,
		  image: undefined,
		},
	  },
	  {
		id: 2,
		ars: 1,
		front: {
		  title: "Austria",
		  content: "What is the capital of Austria?",
		  image: undefined,
		},
		back: {
		  title: "Vienna",
		  content: undefined,
		  image: require(
			  "../assets/example-decks/capitals-of-countries/cards/vienna.jpg"
		  ),
		},
	  },
	],
}
```
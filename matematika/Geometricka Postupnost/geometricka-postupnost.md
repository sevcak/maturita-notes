# Geometrická postupnosť

Postupnosť $\{a_n+1\}$ sa nazýva **geometrická**,
ak existuje také $q C R$, že pre všetky $nCR$  platí:
An+1 = An * q

Číslo **q** sa nazýva **kvocient geometrickej postupnosti**.

V geometrických postupnostiach budeme predpokladať, že:
$A1 =/= 0$

Podľa definície platí:


Predpis pre *n*-tý člen GP:
$a_n = a_1 * q^n-1$

$s_n = a_1 + a_1q ... a_1q^n-1$
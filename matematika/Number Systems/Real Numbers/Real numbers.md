# Real number
-  value of a continuous quantity that can represent a distance along a line
- 0, 1, pi, e

---
# References
[Real number - Wikipedia](https://en.wikipedia.org/wiki/Real_number)

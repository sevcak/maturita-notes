---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
number line ^vuaQ5uSK

2 ^hBkz4Ar2

0 ^cCPqExkH

-2 ^xLke241V

-1 ^PG4NO8dZ

1 ^Yz9Z7oRw


# Embedded files
8d31b189c2869963ddc50c867b2ae6d33e3c1fd7: $$\sqrt{2}$$
1b8dba007c95ad4cf4c9d900ecb7b5edafa84fa1: $$-\frac{1}{2}$$
41c5a5d667f81fb25863e7992449905df659b145: $$\sqrt{-1}$$
e9f3dbcbb687de49079f74c7677de9ec30592adb: $$\sqrt{1}$$

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"type": "text",
			"version": 677,
			"versionNonce": 1454265981,
			"isDeleted": false,
			"id": "vuaQ5uSK",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -793.8680092640641,
			"y": -239.36005815355043,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 70,
			"height": 17,
			"seed": 231481171,
			"groupIds": [
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"fontSize": 13.200000000000022,
			"fontFamily": 1,
			"text": "number line",
			"rawText": "number line",
			"baseline": 12,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "number line"
		},
		{
			"type": "arrow",
			"version": 598,
			"versionNonce": 1927683027,
			"isDeleted": false,
			"id": "A1pY9xeLDpm9BGcCNjdFF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -623.2024007667183,
			"y": -204.9498989890953,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 169.80461512262377,
			"height": 0,
			"seed": 1329777523,
			"groupIds": [
				"ppVYqsi262UMdGMGsizI2",
				"XayhO-CP0b4SN3slFI6k-",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					169.80461512262377,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 776,
			"versionNonce": 720928477,
			"isDeleted": false,
			"id": "1pKITm---9ls7eueNF-6A",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -623.8217958016801,
			"y": -204.9498989890953,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 171.51643708235352,
			"height": 0,
			"seed": 154930461,
			"groupIds": [
				"ppVYqsi262UMdGMGsizI2",
				"XayhO-CP0b4SN3slFI6k-",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-75.8545694196512,
					0
				],
				[
					-171.51643708235352,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 463,
			"versionNonce": 2113481075,
			"isDeleted": false,
			"id": "EcE2V0I4qVp5EmfV8AmpG",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -624.2484462856241,
			"y": -209.96967641647012,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 10.039554854749696,
			"seed": 1712462419,
			"groupIds": [
				"sH8LCmgk7NEG34KRxvtYi",
				"XayhO-CP0b4SN3slFI6k-",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					10.039554854749696
				]
			]
		},
		{
			"type": "text",
			"version": 533,
			"versionNonce": 1486789437,
			"isDeleted": false,
			"id": "cCPqExkH",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -630.7568237515649,
			"y": -185.96770299526742,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 13,
			"height": 20,
			"seed": 1478580211,
			"groupIds": [
				"sH8LCmgk7NEG34KRxvtYi",
				"XayhO-CP0b4SN3slFI6k-",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "0",
			"rawText": "0",
			"baseline": 14,
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "0"
		},
		{
			"type": "line",
			"version": 653,
			"versionNonce": 2056880915,
			"isDeleted": false,
			"id": "tbC9UoFdAt-UT2DHF92cK",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -578.7064269640338,
			"y": -209.96967641647012,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 10.039554854749696,
			"seed": 2060526653,
			"groupIds": [
				"Dd5KhVNmDPjPkH2b2SJef",
				"P45CAnVVokIl-OgtIkXp9",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					10.039554854749696
				]
			]
		},
		{
			"type": "text",
			"version": 734,
			"versionNonce": 939666333,
			"isDeleted": false,
			"id": "Yz9Z7oRw",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -581.903456566627,
			"y": -185.96770299526742,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 6,
			"height": 20,
			"seed": 1694209181,
			"groupIds": [
				"Dd5KhVNmDPjPkH2b2SJef",
				"P45CAnVVokIl-OgtIkXp9",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "1",
			"rawText": "1",
			"baseline": 14,
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "1"
		},
		{
			"type": "line",
			"version": 595,
			"versionNonce": 1631430835,
			"isDeleted": false,
			"id": "JJx-binp2Vx64IvsjAYrI",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -526.8969774172812,
			"y": -209.96967641647012,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 10.039554854749696,
			"seed": 2127241501,
			"groupIds": [
				"Dbtvp1GOi_ow6uJhpgbTn",
				"P45CAnVVokIl-OgtIkXp9",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					10.039554854749696
				]
			]
		},
		{
			"type": "text",
			"version": 666,
			"versionNonce": 424980477,
			"isDeleted": false,
			"id": "hBkz4Ar2",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -533.4053548832218,
			"y": -185.96770299526742,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 13,
			"height": 20,
			"seed": 693492125,
			"groupIds": [
				"Dbtvp1GOi_ow6uJhpgbTn",
				"P45CAnVVokIl-OgtIkXp9",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "2",
			"rawText": "2",
			"baseline": 14,
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "2"
		},
		{
			"type": "line",
			"version": 675,
			"versionNonce": 1468799571,
			"isDeleted": false,
			"id": "ZvG28JCv_6Wuwcy082pDO",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -670.0729623028487,
			"y": -209.96967641647012,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 10.039554854749696,
			"seed": 1978970973,
			"groupIds": [
				"CW0yfEqNosz3MTrFaQMFT",
				"P45CAnVVokIl-OgtIkXp9",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					10.039554854749696
				]
			]
		},
		{
			"type": "text",
			"version": 747,
			"versionNonce": 1251453021,
			"isDeleted": false,
			"id": "PG4NO8dZ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -676.5813397687895,
			"y": -185.96770299526742,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 13,
			"height": 20,
			"seed": 1327513533,
			"groupIds": [
				"CW0yfEqNosz3MTrFaQMFT",
				"P45CAnVVokIl-OgtIkXp9",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "-1",
			"rawText": "-1",
			"baseline": 14,
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "-1"
		},
		{
			"type": "line",
			"version": 712,
			"versionNonce": 173758451,
			"isDeleted": false,
			"id": "DC4epLXPT9HNXoNyuFDc0",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -712.7897840196974,
			"y": -209.96967641647012,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 10.039554854749696,
			"seed": 1021106483,
			"groupIds": [
				"el6AT87Q1_UjDzrPcsFhu",
				"P45CAnVVokIl-OgtIkXp9",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					10.039554854749696
				]
			]
		},
		{
			"type": "text",
			"version": 794,
			"versionNonce": 1570542781,
			"isDeleted": false,
			"id": "xLke241V",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -723.8512647977407,
			"y": -185.96770299526742,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 20,
			"height": 20,
			"seed": 1223768787,
			"groupIds": [
				"el6AT87Q1_UjDzrPcsFhu",
				"P45CAnVVokIl-OgtIkXp9",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "-2",
			"rawText": "-2",
			"baseline": 14,
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "-2"
		},
		{
			"type": "line",
			"version": 919,
			"versionNonce": 1202839955,
			"isDeleted": false,
			"id": "X3c05T_6Mtc5sD99nPsqx",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -560.4522523478776,
			"y": -209.96967641647012,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 10.039554854749696,
			"seed": 1808785469,
			"groupIds": [
				"-yBqd2MTkUD3V2kXSpMdj",
				"AnJoyO3siQKT7MMf3m8oe",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					10.039554854749696
				]
			]
		},
		{
			"type": "image",
			"version": 597,
			"versionNonce": 772076829,
			"isDeleted": false,
			"id": "Rv-auK1psYyD8wHx-CHeL",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -569.2280504235196,
			"y": -229.57286037924342,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 16.534538638529067,
			"height": 13.089843088835512,
			"seed": 1052954781,
			"groupIds": [
				"AnJoyO3siQKT7MMf3m8oe",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "8d31b189c2869963ddc50c867b2ae6d33e3c1fd7",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "line",
			"version": 1005,
			"versionNonce": 1489385267,
			"isDeleted": false,
			"id": "D4dvR4M-8l77xU6CL-M9E",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -654.6460214293462,
			"y": -209.96967641647012,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 10.039554854749696,
			"seed": 1393147069,
			"groupIds": [
				"SysNlwt1fYprVnIpW0Ms3",
				"ploWX3LF_N64tRLc8R5fB",
				"AnJoyO3siQKT7MMf3m8oe",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					10.039554854749696
				]
			]
		},
		{
			"type": "image",
			"version": 400,
			"versionNonce": 658655613,
			"isDeleted": false,
			"id": "GVtjA7EU",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -666.1537087149809,
			"y": -231.8664670906803,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 16.33389098261946,
			"height": 16.48568738688204,
			"seed": 25831,
			"groupIds": [
				"AnJoyO3siQKT7MMf3m8oe",
				"Sn--zzqnxIarZzufC6FQx",
				"KQnTK4JQq9Aey8H_5ACpP",
				"YyWm8XAho0Wmga7gS2Kyf",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780152862,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "1b8dba007c95ad4cf4c9d900ecb7b5edafa84fa1",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "arrow",
			"version": 290,
			"versionNonce": 973749331,
			"isDeleted": false,
			"id": "crZsPgSaSQysWoPMw7UHN",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -624.0173343369086,
			"y": -251.2675015983566,
			"strokeColor": "#d9480f",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 50.96086914427781,
			"seed": 1830506963,
			"groupIds": [
				"X8BaB5mj-rjr8FLEQ_51O",
				"DLFxXW_vKd6nfpCFrMEUD",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780110932,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "triangle",
			"points": [
				[
					0,
					0
				],
				[
					0,
					-25.480434572138904
				],
				[
					0,
					-50.96086914427781
				]
			]
		},
		{
			"type": "image",
			"version": 691,
			"versionNonce": 1700526803,
			"isDeleted": false,
			"id": "g9RLSvLT-nFhMolVtjtZn",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -609.875330795147,
			"y": -285.6581856749118,
			"strokeColor": "#d9480f",
			"backgroundColor": "transparent",
			"width": 36.71541362738461,
			"height": 17.886996382571997,
			"seed": 672636787,
			"groupIds": [
				"X8BaB5mj-rjr8FLEQ_51O",
				"DLFxXW_vKd6nfpCFrMEUD",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780114134,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "41c5a5d667f81fb25863e7992449905df659b145",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "arrow",
			"version": 544,
			"versionNonce": 2093300211,
			"isDeleted": false,
			"id": "1INWZWluP1r_s9Cm3WgaW",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -624.0173343369086,
			"y": -145.1890928304656,
			"strokeColor": "#d9480f",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 56.947575820896304,
			"seed": 276022045,
			"groupIds": [
				"UlYcPnC8D7-B2JCljaIR-",
				"-01bhMyg4qz6I_uCKnd6W",
				"DLFxXW_vKd6nfpCFrMEUD",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1665780110932,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "0m09hPS4",
				"focus": 2.2322309513933,
				"gap": 14.786771416719603
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "triangle",
			"points": [
				[
					0,
					0
				],
				[
					0,
					56.947575820896304
				]
			]
		},
		{
			"type": "image",
			"version": 111,
			"versionNonce": 146395837,
			"isDeleted": false,
			"id": "0m09hPS4",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -609.230562920189,
			"y": -132.60422564356602,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 24,
			"height": 19,
			"seed": 74656,
			"groupIds": [
				"-01bhMyg4qz6I_uCKnd6W",
				"DLFxXW_vKd6nfpCFrMEUD",
				"lsm9-tMuYU0RC2_l0Xazu"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "1INWZWluP1r_s9Cm3WgaW",
					"type": "arrow"
				}
			],
			"updated": 1665780110932,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "e9f3dbcbb687de49079f74c7677de9ec30592adb",
			"scale": [
				1,
				1
			]
		},
		{
			"id": "Lr5P92gc",
			"type": "text",
			"x": -645.8055092640641,
			"y": -101.85994387610185,
			"width": 10,
			"height": 20,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1998842521,
			"version": 3,
			"versionNonce": 6107001,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1665907085814,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 16,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "top",
			"baseline": 14,
			"containerId": null,
			"originalText": ""
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "#f1f3f5",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 16,
		"currentItemTextAlign": "center",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "triangle",
		"currentItemLinearStrokeSharpness": "sharp",
		"gridSize": null,
		"colorPalette": {}
	},
	"files": {}
}
```
%%
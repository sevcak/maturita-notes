# Imaginary Unit and Imaginary Numbers

Some quadratic equations do not have any [[Real numbers|real number]] solutions.

There's no [[Real numbers|real number]] solution to the equation $x^2 = -1$.
However, a solution does exist in a new number system called the [[Complex Numbers | complex number system]].
The backbone of this new number system is the **imaginary unit**, or the number $i$.

## Imaginary unit

- not present on a one-dimensional continuum (the number line)

![[drawing-imaginary-numbers.excalidraw.svg]]

### $i$ = imaginary unit

**Definition:**
$i^2 = -1$
$i = \sqrt{-1}$

**Derived:**
$i^0 = 1$
$i^1 = i$
$i^2 = -1$
$i^3 = {i^2}\times{i} = {-1}\times{i} = -i$ 
$i^4 = {i}\times{i^3} = {i}\times{-i} = {(-1)}\times{(i)}\times{(i)} = {(-1)}\times{(-1)} = 1$
$i^5 = {i^4}\times{i} = {1}\times{i} = i$
$i^6 = {i^5}\times{i} = {i}\times{i} = i^2 = -1$
$i^7 = {i^6}\times{i} = {(-1)}\times{i} = -i$
$i^8 = {i^7}\times{i} = {(-i)}\times{i} = {(-1)}\times{(i)}\times{(i)} = {(-1)}\times{(-1)} = 1$
**This keeps cycling back**

## Pure imaginary numbers

$i$ is by no means alone.
By taking multiples of $i$, it is possible to create ininitely many more **pure imaginary numbers**.

Numbers of the form $bi$ , where $b$ is a nonzero [[Real numbers|real number]]:
- $3i$
- $i\sqrt{5}$
- $-12i$
How they relate to the [[Real numbers|real numbers]]:
$(3i)^2 = 3^2i^2 = 9i^2 = 9(-1) = -9$
The fact that $(3i)^2$ means that $3i$ is a square root of -9.

**Pure imaginary numbers are the square roots of negative numbers.**

*For $a > 0, \sqrt{-a} = i\sqrt{a}$*

---

# Resources
- [Imaginary Numbers Are Real - YouTube](https://www.youtube.com/watch?v=T647CGsuOVU)
- [Introduction to i and imaginary - YouTube](https://www.youtube.com/watch?v=ysVcAYo7UPI)
- [Intro to the imaginary numbers - Khan Academy](https://www.khanacademy.org/math/algebra2/x2ec2f6f830c9fb89:complex/x2ec2f6f830c9fb89:imaginary/a/intro-to-the-imaginary-numbers?modal=1)
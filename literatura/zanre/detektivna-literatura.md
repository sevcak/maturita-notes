# 🕵️ Detektívna literatúra

Detektívka je literárny žáner, ktorý sa zameriava na riešenie zločinov a tajomstiev. Je plná napätia, intrigy a záhadných postáv, ktoré sú zvyčajne postavené pred zdanlivo neriešiteľné situácie. Tento žáner literatúry sa stal obľúbeným medzi čitateľmi rôznych vekových kategórií, pretože ponúka zážitok z rozplétania zápletky a hľadania pravdy.

---

- [[epika|epický]] literárny žáner

- 🔎 vyšetrovanie zločinov, odhaľovanie tajomstiev.
- štruktúra záhady, deduktívne myslenie.

- ❓ Prekvapenia, zvraty, zaujímavosť.
- ⭐ Populárny žáner, verní fanúšikovia, neustály vývoj.

- postavy: 🕵️[[#🕵️ Detektív|detektívi]], obete, podozriví.
- témy: kriminalita, spravodlivosť, etika, korupcia.

---

# 👤 Postavy

## 🕵️ Detektív

Základným prvkom detektívky je detektív, ktorý je postavený do úlohy vyriešiť záhadu. 
Detektívi majú rôzne charakteristiky a štýly práce, no zvyčajne sú vykreslení ako inteligentní, pozorovatelia a analytici, ktorí dokážu dedukovať zo stop a dôkazov pravdu a identifikovať páchateľa.
Detektív sa zvyčajne spolieha na svoj intelekt, logické myslenie a často aj na svoje skúsenosti a intuíciu.

### Príklady detektívov

**Sherlock Holmes**
- britský detektív
- brilantné deduktívne schopnosti, pozorovacie schopnosti, vysoký intelekt a logické myslenie
- excentrický, s výrazným charakterom

**Hercule Poirot**
- belgický detektív
- brilantné deduktívne schopnosti, výrazné myslenie
- muž s pevnými presvedčeniami, eleganciou

**Harry Hole**
- detektív z Osla
- typický hard-boiled detektív
-  zložitý a rozpoltený charakter s mnohými vnútornými démonmi
- nepredvídateľný a s tendenciou k sebaškodlivému správaniu

---

## ✍️ Autori

Za zakldateľa detektívky sa považuje [[#Edgar Allan Poe]] (Vraždy v ulici Morgue)

ďalší známi autori:
- Agatha Christie
- Arthur Conan Doyle
- Edgar Allan Poe
- Edogawa Ranpo
- Jo Nesbø


### Edgar Allan Poe
*(1809 - 1849)*

- americký spisovateľ, básnik a redaktor
- 
# 🎭 Dráma

*(gr. dro - činiť, konať)*

- literárny druh
- napísana formou určenou pre [[#Divadelná hra|divadelné predstavenie]]
- vyvýja sa v dobe [[anticka-literatura|antickej]]

---

# Stavba drámy

## Vonkajšia stavba drámy

1. **dejstvá**
	- časti hry oddelené spúštaním opony
1. **scény**
	- zmena demokracií/kulís
1. **výstupy**

---

# Dramatické žánre

## Základné dramatické žánre

- komédia
- [[#Tragédia|tragédia]]


### Tragédia

- nešťastný príbeh
- často končí smrťou väčšiny alebo všetkých postáv

**Klasická tragédia:**
1) Expozícia
2) Kolízia
3) Kríza
4) Peripetia
5) Katastrofa
- Katarzia 

## Súčasné žanre

### Smutné formy

- Melodráma

### Zábané dramatické formy

- **estráda** - osviežiť a relaxovať
- **kabaret**
- **talk show**

- bábková hra
- veselohra

---

## Divadelná hra

- realizácia divadelného diela - [[#Dráma|drámy]]
- realizuje sa na špecialnom priestore - **javisko**

### Inscenácia
Naštudovanie divadelného diela do divadelnej hry 

### Premiéra
Prvé uvedenie divadelnej hry

### Repríza
druhé uvedenie 

### Derniéra
Posledné uvedenie divadelnej hry

---

# Ďalšie pojmy

## Hudobné žánre

- opera
- balet
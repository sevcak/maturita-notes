# Epika

Epika je jeden z hlavných literárnych žánrov, ktorý sa zameriava na vyprávanie príbehov. Je to žáner, ktorý sa vyznačuje rozsiahlymi, často dobrodružnými príbehmi, v ktorých sa zväčša stretávame s hrdinskými postavami a ich dobrodružstvami, a kde sa dianie zväčša odohráva v rôznych časových obdobiach a miestach.

---

- hlavný **literárny druh**
- **dejovosť** - má **príbeh** - o hrdinoch, dejoch a udalostiach
- prozaická forma
- má rozprávača
# William Shakespeare - Hamlet

> „Byť, a či nebyť - kto mi odpovie, čo šľachtí ducha viac: či trpne znášať strely a šípy zlostnej Šťasteny, či pozdvihnúť zbraň proti moru bied a násilne ho zdolať?“
> 
> ~ *Hamlet*
---

## Charakteristika

- autor: **William Shakespeare**
- lit. druh: [[drama|dráma]]
- obdobie: **humanizmus a renesancia**
- žáner: **tragédia**
- téma: **túžba po moci**, **pomste**, **úvahy o existencii**, **rodinný konflikt prerastá v obecne spoločenský konflikt**
- miesto: **Dánsko**, hrad **Elsinor**
- čas: **začiatok 17. storočia**

- postavy:
	- **Hamlet** - hl. postava
		- dánsky princ
	- **Ofélia**
		- Hamletova milá
		- dcéra Polónia
	- **Gertrúda**
		- Hamletova matka
		- kráľovná
	- **Horatio**
		- Hamletov priateľ
		- Jediný, kto pozná Hamletove plány
	- **Claudius**
		- Hamletov strýko
		- vrah Hamletovho otca
		- zákerný, podlý
		- kráľ, vzal si Hamletovu matku
	- **Polonius**
		- otec Ofélie
		- radca kráľa
		- verný kráľovi
	- **Laertes**
		- Poloniov syn
		- pomctichtivý
		- šermia
		- šermia

---

## Dej

- Hamletovi duch prezradí, že jeho otec bol zavraždený
- Hamletova matka sa vydáva za jeho strýka, je so smrťou Halmetovho otca stotožnená
- Hamlet omylom zabíja otca Ofélie
- Ofélia pácha samovraždu
- Všetci okrem Horatia zomierajú
	- Hamlet prosí Horatia, aby sa nezabil, ale prerozprával príbeh
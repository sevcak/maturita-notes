# Molière - Lakomec

> „Ach peniaze, peniaze, vy moje poklady. Ste preč!“
> 
> *~ Harpagon*
---

## Charakteristika

- autor: **Molière**
- lit. druh: [[drama|dráma]]
- obdobie: **klasicizmus**
- žáner: **komédia**
- téma: **nenásytný lakomec sa chce oženiť s mladým dievčaťom, do ktorého je zaľubený jeho vlastný syn**
- Idea: **lakomec začne konať len keď hrozí, že príde o svoje peniaze.**
- miesto: **Dánsko**, hrad **Elsinor**
- čas: **začiatok 17. storočia**

- postavy:
	- **Harpagon** - hl. postava
		- bohatý, ale lakomý a nenásytný
		- otec Kleanta a Elizy
	- **Kleant**
		- Harpagonov syn
		- dcéra Polónia
	- **Gertrúda**
		- Hamletova matka
		- kráľovná
	- **Horatio**
		- Hamletov priateľ
		- Jediný, kto pozná Hamletove plány
	- **Claudius**
		- Hamletov strýko
		- vrah Hamletovho otca
		- zákerný, podlý
		- kráľ, vzal si Hamletovu matku
	- **Polonius**
		- otec Ofélie
		- radca kráľa
		- verný kráľovi
	- **Laertes**
		- Poloniov syn
		- pomctichtivý
		- šermia
		- šermia

---

## Dej

- Hamletovi duch prezradí, že jeho otec bol zavraždený
- Hamletova matka sa vydáva za jeho strýka, je so smrťou Halmetovho otca stotožnená
- Hamlet omylom zabíja otca Ofélie
- Ofélia pácha samovraždu
- Všetci okrem Horatia zomierajú
	- Hamlet prosí Horatia, aby sa nezabil, ale prerozprával príbeh
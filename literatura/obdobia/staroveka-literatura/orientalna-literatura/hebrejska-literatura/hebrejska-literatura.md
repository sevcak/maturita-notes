# Hebrejská literatúra

- oblasť Blízkeho východu, židovské kráľovstvo
- pôvod [[biblia|Biblie]]

### Znaky
- monoteizmus - Boh
- kult duše
- dôraz na nadpozemský život
- formovanie spoločenských a mravných zásad

### Príklad
- [[biblia|Biblia]]

# Epos o Gilgamešovi

- hrdinský epos
- koláž starobabylonských mýtov

### Postavy
- Gilgameš - poloboh, vládca mesta Uruk
- Enkidu - divoch, ktorý sa vedel vyrovnať Gilgamešovi

### Dej
- obyvatelia mesta Uruk prosia bohov o niekoho, kto sa vyrovná Gilgamešovi
- bohovia vytvoria divocha Endidua, ktorý vyzve Gilgameša
- Gilgameš a Enkidu sa spriatelia po oboch uznaní kvality súpera
- Enkidu zomrie a Gilgameš sa vydáva hľadať slávu a nesmrteľnosť

### Myšlienka
- nesmrteľnosť spočíva v slávnych skutkoch
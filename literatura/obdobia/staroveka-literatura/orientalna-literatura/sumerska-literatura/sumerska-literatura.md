# Sumerská literatúra

- najstaršia zachovaná literatúra
- staroveká Mezopotámia
- klinové písmo, hlinené tabuľky

### Znaky
- polyteizmus
- bohovia a polohovia majú kladné a záporné vlastnosti - odrážajú ľudí
- symbolika postáv a predmetov
- rozsiahle epické diela - **eposy**

### Príklad
- [[epos-o-gilgamesovi|Epos o Gilgamešovi]] - hrdinský epos

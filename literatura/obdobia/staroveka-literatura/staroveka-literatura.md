# Staroveká literatúra

- 3 000 pred Kr. - 500

### Spoločensko-historická situácia
- polyteizmus / monoteizmus
- **prvé civilizácie**
- zrod právnych noriem
- **vznik písma**

### Znaky
- postavy: bohovia, polobohovia, panovníci, ľudia s chybami
- témy:
	- vznik sveta, človeka, rastlín
	- posmrtný život
	- pamätné udalosti
- príbehy podávane ústne potulníkmi - **rapsódmi**

### Národné literatúry

1. Orientálna literatúra
	1. [[sumerska-literatura|Sumerská literatúra]]
	2. [[hebrejska-literatura|Hebrejská literatúras]]
4. [[anticka-literatura|Antická literatúra]]
	1. [[grecka-staroveka-literatura|Grécka literatúra]]
	2. [[rimska-staroveka-literatura|Rímska literatúra]]
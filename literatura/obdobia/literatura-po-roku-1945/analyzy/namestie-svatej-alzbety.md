# Rudolf Jašík - Námestie svätej Alžbety

## Charakteristika

- lit. druh: **epika**
- lit. žáner: **sociálny román**
	- vzťahy a pomery medzi ľudmi
- obdobie: **2. sv. vojna**
- miesto: **Česko-slovensko**
	- 
- téma: **Tragický príbeh Igora a Evy**
- postavy:
	- **Igor Hamar** - **hl. postava**
		- kresťanský chlapec
		- robí príležisotsné práce
		- je príliš mladý, aby šiel do vojny
		- zodpovedný
	- **Eva Weinmannová** - **hl. postava**
		- židovka, musí nosiť hviezdu
	- **Samko Weinmann**
	- **Maxi Schlesinger**
	- **Jozef Maguš**
	- **Flórik**
	- **Dodo**
	- **Haso**

## Dej

- Stretnú sa dvaja ľudia z rôznych svetov, Igor a Žltý Dodo
- Igor má milú - Evu Weinmannovú
- Príde chvíľa, keď Eva má byť deportovaná
	- Igor jej ponúka záchranu sobášom
	- Stálo to veľa, všetci sa snažia nazbierať peniaze
		- Maxi predáva koňe
- Farár za zľakne a odmieta, Eva je deportovaná
- Židia čakajú na stanici, Igor sa chce ešte s Evou stretnúť
- Igor vidí:
	- že židia nesmú vystúpiť z vagónov
	- ako malého chlapca zastrelia a jeho matka sa na to pozerá
	- ako Eva vystupuje z vagónu, aby vzala telo chlapca a zastrelia ju
- Igor viní Flórika, chce sa zabiť
- Prichádza muž, presvedčí ho, aby žil
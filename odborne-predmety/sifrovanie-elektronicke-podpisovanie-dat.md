# Šifrovanie a elektronické podpisovanie dát

## Rozdiel medzi kódovaním a šifrovaním
- pri šifrovaní utajujeme informácie
- pri šifrovaní máme **šifrovací kľúč**

## Steganografia
- ukrývanie správ

## Symetrické šifrovanie
- jeden kľúč aj pre šifrovanie aj odšifrovanie

## Asymetrické šifrovanie
- verejný a súkromný kľúč - **kľúčový pár**
- jeden kľúč šifruje, druhý odšifruje
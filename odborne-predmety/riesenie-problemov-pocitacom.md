# Vysvetli pojmy

## Arcitektura
- vysvetli rozdiely medzi **Von Neumannovou** a **Harvardskou arcitekturou**

## Algoritmizacia
- algoritmus
- riadiace štruktúry v algoritmoch
    - cykly, switche, podmienky

## Rekurzia
- funkcia, ktorá vola samú seba
- musí mať konečnú podmienku
- pri nedosiahnutí konečnej podmienky vzniká **stack overflow**


## Kompilátory
- vysvetli, čo je to kompilátor

## Druhy jazykov
- z hľadiska kompilácie:
	- kompilovaný
	- interpretovany
	- priklady:
		- Java - kompilovaný na bytecode a potom interpretovaný
- z hľadiska úrovne:
	- nízkoúrovňový
		- je blízko ku komunikácii priamo s hardvérom
	- vysokoúrovňový
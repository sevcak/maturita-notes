
# Výber mobilného zariadenia

## V čom sa líšia mobilné zariadenia od nemobilných

- pevne spojený displej
- integrované zariadenia 
- z hľadiska vymeniteľnosti komponentov
	- možnosti rozšírenia
- notebook, tablet, čítačka...
	- porovnať - účel, operačný systém
- zobrazovacie technológie
	- OLED
	- LCD
	- eInk
- možnosti bezdrôtového prenosu dát
	- bluetooth
	- monilné dáta
- obhájenie svojho výberu

## Napájanie

### Čo je to elektromagnetická indukcia
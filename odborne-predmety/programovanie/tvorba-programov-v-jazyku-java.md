# Java

- programovací jazyk:
	- vysokoúrovňový
	- kompilovaný na bytecode, potom interpretovaný pomocou [[#Java Virtual Machine (JVM)|JVM]]
	- objektovo-orientovaný
	- cross-platformový
		- bytecode sa interpretuje rôznymi verziami [[#Java Virtual Machine (JVM)|JVM]]

## Java Virtual Machine (JVM)
- Java ineterpreter
- rôzny pre každú platformu
- Java program dokáže spustiť každé zariadenie s JVM

## Rozdiel medzi OOP a funkcionálnym programovaním

# Pojmy OOP

## Trieda
- programátorom zadefinovaný dátový typ
- zapuzdruje dátove položky a [[#Metóda|metódy]]

## Objekt
- dátový typ typu [[#Trieda|trieda]]
- **inštancia**

## Dátová položka
- predstavuje vlastnosti objektu triedy

## Metóda
- procedúra, ktorou sa komunuikuje s objektami

### Statická metóda
- nevyžaduje vytvorenie objektu triedy pre jej použitie

## Zapuzdrenie

## Vkladanie

## Dedenie

## Konštruktor

### Implicitný konštruktor
- vždy sa volá pri vytvorení objekta

## Kľúčove slová a modifikátory

### this


### super
- odkaz na rodičovskú triedu
- používané aj pri volaní konštruktora z konštruktora

### Modifikátory prístupu
`public`
`friendly`
`protected`
`private`

## Formátovaný výstup
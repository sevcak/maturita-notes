## Jazyk C
- co to je ?
- proceduralny
- nizkourovnovy

## Syntax
- boilerplate
    - int main(argc, argv*[]) ...blablabla...
- funkcia
- vyrazy
    - priradenie
    - logicke vyrazy
    - deklaracia

## Funkcia
- funkciami rozdeluje velky problem na viacero mensich
- robenie vlastnych funkcii v C
    - deklaracia
    - definicia
    - volanie
- telo funkcie
- blok prikazov - {}
- navratovy typ funkcie

## Premenna
- symbolicky nazov pre adresu vyhradenu v pamati pocitaca
- datove typy
- oblast platnosti
    - globalne
    - localne (nested scope)

## Pointer (smerník)
- premenná, ktorá odkazuje na inú premennú
- obsahuje adresu inej premennej
- referencia = odkaz
- operátory
    - `&` - **referenčný operátor**
          - pristupujeme ním k adrese pointera
    - `*` - **dereferenčný operátor**
          - pristupujeme ním k hodnote premennej, na ktorú pointer odkazuje

## Pole
- **premenná obsahujuca viac premenných rovnakého dátoveho typu** pod jednym názvom
- **indexovanie**
    - od **0** po **dĺžku poľa - 1**
```c
//               0    1    2    3
char pole[4] = {'A', 'h', 'o', 'j'};

printf("%i", pole[0]); // A
printf("%i", pole[1]); // h
printf("%i", pole[2]); // o
printf("%i", pole[3]); // j
```

### Štruktúra - struct
- programátorom zadefinovaný zložený dátový typ
- skladá sa z už existujúcich dátových typov

## Subor
- štruktura
- otvorenie suboru
```c
fopen()
```
	- prástupové operátory
	- r - read
	- w - write
	- a - append
- pristupove prava k suborom v Linuxe:
    - r - read
    - w - write
    - e - execute
    - prava pre roznym pristupovatelom:
        - owner
        - group
        - others

## Formátovaný výstup


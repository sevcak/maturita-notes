# Tranzistor

## Bipolarny tranzistor
- vyrobeny z polovodica
- tri vrstvy
- tri casti:
    - emitor - prepusta prud von
    - kolektor - prijima prud dnu
    - baza - ovlada, kolko prudu prejde z kolektora do emitora
- dve rezimy:
    - zosilnovac
    - spinac (logicka 1 alebo 0)
- zapojenie so spolocnym emitorom

## MOSFET tranzistor
- unipolárny tranzistor - ovládani
- 3 časti:
	- source
	- gate - hradlo
	- drain
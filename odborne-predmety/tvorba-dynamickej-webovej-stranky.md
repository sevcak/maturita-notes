Popíšte praktické využitie logických operátorov

Popíšte, ako sa používajú databázy

## Čo je Worldwide Web
- služba sprístupňujúca obsah vo forme hypertextu

## Webový klient
- spracovávač príkazov, interpretuje ich do grafickej podoby

## Webový server
- zariadenie, ktoré poskytuje a drží obsah

## Popíš jazyk CSS a jeho využitie
- druhy pripojenia
	- inline - `style` prop
	- externý súbor - link v html
	- v hlavičke html - `style`
- syntax
```css
selector {
	vlastnost: hodnota;
}
```
- pseudo triedy
	- `:hover`

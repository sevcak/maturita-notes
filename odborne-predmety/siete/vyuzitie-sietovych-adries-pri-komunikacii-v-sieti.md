## Popíš TCP Model
- berie z [[prenos-dat-pomocou-pocitacovych-sieti#ISO OSI RM|ISO OSI]], čo potrebuje
- 4 vrstvy:
	1) **Aplikačná**
		- ISO OSI ekvivalent aplikačnej, prezenčnej a relačnej vrstvy
	2) **Transportná**
		- rieši OS
	3) **Internetová**
	4) **Prístup do siete**

## IPv4 Adresa
- 32 bitov
- zápis zjednodušený pre človeka
```
 137    .    11    .     24     .     62
  |          |           |            |
0-255      0-255       0-255        0-255
```
### IPv4 Maska
- pri vytváraní siete sa berú bity od konca podľa rozmeru siete
- napr `255.255.255.0` necháva na počítače posledných 256 (254 bez adresy siete a broadcastu)
- `/x` - x prvých bytov musia mať všetky počítače v sieti spoločných

## IPv6 Adresa
- 128 bitov
- zapisuje sa v 16-kovej sústave
- `FECO:CAFE:FOOD:BEEF:BAD`

## localhost
- doma

## Najpoužívanejšie TCP a UDP portov

### `:80`
- http apache

### `:20`, `:21`
- FTP
- jeden na príkazy
- jeden na dáta



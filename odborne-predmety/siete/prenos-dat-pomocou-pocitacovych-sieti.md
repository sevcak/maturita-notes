
# Počítačová sieť

- skupina zariadení prepojených sieťovými médiami 

## Druhy zariadení v sieti
- koncové
	- klient
- poskytujúce
	- server
- sprostredkujúce
	- switch - prepája zariadenia v jednej sieti
	- router - prepája sieťe medzi sebou

## Druhy prenosov dát
- drôtový
- bezdrôtový

## Komunikačný protokol
- súbor pravidiel posielania dát sieťou
- môže obsahovať pravidlá o:
	- začiatku a nadväzovaní komunikácie
		- napr.  telefónny hovor sa nadviaže:
			- vytočením čísla
			- komunikáciou so satelitom
			- komunikáciou satelitu s cieľovým zariadením
			- začatie zvonenia telefónu cieľového zariadenia
			- pri prijatí hovoru sa prenáša hlas frekvenciou

## ISO OSI RM
- referenčný model
- všeobecné popisuje komunikáciu
- všetko, čo spolu komunikuje sa ním dá popísať

### 7 vrstiev ISO OSI
7) **aplikačná**
	- berie dáta a začína ich pripravovať pre sieť
	- posiela pripravené dáta na nižšiu vrstvu
6) **prezentačná**
	- má za úlohu správu dát
	- stará sa o čo najlepšie odprezentovanie dát adresátovi
5) **relačná**
	- nadviazanie spojenia
	- udržiavanie spojenia nažive - keep alive
	- končí spojenie
4) **transportná**
	- stará sa o to, aby sa dáta nemiešali
	- idenfikuje aplikácie
	- posiela dáta správnym aplikáciam
	- seká dáta na segmenty
		- na spoločný kábel k sprostredkovateľovi postupne sa posielajú každého dáta po kúskoch v balíčkoch - packetoch
3) **sieťová**
	- identifikuje zdroj a cieľ pomocou adries
	- prenáša dáta medzi zdrojom a cieľom
	- nerieši obsah balíčku
	- snaží sa čo najrýchlejšie packet doručiť - hľadá najkratšiu cestu
2) **spojová**
	- pripravuje dáta na prenos po konkrétnom médiu
	- zabezpečuje komunikáciu v lokálnej sieti	
	- vytvára rámce - framy
	- MAC adresácia
1) **fyzická**
	- fyzikálne žáležitosti
	- vzhľad, rozmer, tvar konektorov
	- napätie reprezentujúce jednotky alebo nulky


## IP adresa
- identifikuje zdroj a cieľ

## MAC adresa
- adresovanie o úroveň nižšie ako IP
- pomáha určit, komu najbližšie poslať dáta po sieti

## DNS
- služba priraďujúca symbolické, zapamätateľné názvy domien pre adresy
- pomocou URL adresy sa zistí adresa poatriaca k domén